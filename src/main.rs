mod logic;
mod render;

use crate::logic::{GameOfLife, GOL_SIZE};
use crate::render::{render_gol, PIXEL_SIZE};
use raylib::prelude::*;

fn main() {
    let (mut rl, thread) = raylib::init()
        .size(
            (PIXEL_SIZE * GOL_SIZE) as i32,
            (PIXEL_SIZE * GOL_SIZE) as i32,
        )
        .title("Game Of Life")
        .build();
    let mut gol = GameOfLife::new();
    // Glider
    gol.cells[1][0] = true;
    gol.cells[2][1] = true;
    gol.cells[2][2] = true;
    gol.cells[2][2] = true;
    gol.cells[1][2] = true;
    gol.cells[0][2] = true;
    // Blinker
    gol.cells[1][7] = true;
    gol.cells[1][8] = true;
    gol.cells[1][9] = true;
    while !rl.window_should_close() {
        gol.run_generation();
        let mut d = rl.begin_drawing(&thread);
        d.clear_background(Color::BLACK);
        render_gol(&gol, &mut d);
        d.draw_fps(10, 10);
    }
}
