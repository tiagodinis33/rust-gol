pub const GOL_SIZE: usize = 50;

pub struct GameOfLife {
    pub cells: [[bool; GOL_SIZE]; GOL_SIZE],
}
impl GameOfLife {
    pub fn new() -> Self {
        return Self {
            cells: [[false; GOL_SIZE]; GOL_SIZE],
        };
    }
    fn live_neighbor_count(&self, row: usize, column: usize) -> u8 {
        let mut count = 0;
        for delta_row in [GOL_SIZE - 1, 0, 1].iter().cloned() {
            for delta_col in [GOL_SIZE - 1, 0, 1].iter().cloned() {
                if delta_row == 0 && delta_col == 0 {
                    continue;
                }

                let neighbor_row = (row + delta_row) % GOL_SIZE;
                let neighbor_col = (column + delta_col) % GOL_SIZE;
                if self.cells[neighbor_col][neighbor_row] {
                    count += 1;
                }
            }
        }
        count
    }
    pub fn run_generation(&mut self) {
        let mut next = self.cells.clone();

        for row in 0..GOL_SIZE {
            for col in 0..GOL_SIZE {
                let cell = self.cells[col][row];
                let live_neighbors = self.live_neighbor_count(row, col);

                let next_cell = match (cell, live_neighbors) {
                    // Rule 1: Any live cell with fewer than two live neighbours
                    // dies, as if caused by underpopulation.
                    (true, x) if x < 2 => false,
                    // Rule 2: Any live cell with two or three live neighbours
                    // lives on to the next generation.
                    (true, 2) | (true, 3) => true,
                    // Rule 3: Any live cell with more than three live
                    // neighbours dies, as if by overpopulation.
                    (true, x) if x > 3 => false,
                    // Rule 4: Any dead cell with exactly three live neighbours
                    // becomes a live cell, as if by reproduction.
                    (false, 3) => true,
                    // All other cells remain in the same state.
                    (otherwise, _) => otherwise,
                };

                next[col][row] = next_cell;
            }
        }

        self.cells = next;
    }
}
