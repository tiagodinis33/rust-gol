use crate::logic::{GameOfLife, GOL_SIZE};
use raylib::color::Color;
use raylib::prelude::*;
pub const PIXEL_SIZE: usize = 10;
pub fn render_gol(gol: &GameOfLife, d: &mut RaylibDrawHandle) {
    for x in 0..GOL_SIZE {
        for y in 0..GOL_SIZE {
            let is_alive = gol.cells[x][y];
            let color = if is_alive { Color::WHITE } else { Color::BLACK };
            d.draw_rectangle(
                (x as usize * PIXEL_SIZE) as i32,
                (y as usize * PIXEL_SIZE) as i32,
                PIXEL_SIZE as i32,
                PIXEL_SIZE as i32,
                color,
            );
        }
    }
}
